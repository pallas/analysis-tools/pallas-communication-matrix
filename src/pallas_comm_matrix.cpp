/*
 * Copyright (C) Telecom SudParis
 * See LICENSE in top-level directory.
 */
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <iomanip>
#include <cassert>

#include <pallas/pallas.h>
#include <pallas/pallas_archive.h>
#include <pallas/pallas_read.h>
#include <pallas/pallas_storage.h>


void pop_data(pallas::Event *e, void *data, size_t data_size, byte *&cursor) {
    if (cursor == nullptr) {
        /* initialize the cursor to the begining of event data */
        cursor = &e->event_data[0];
    }

    uintptr_t last_event_byte = ((uintptr_t) e) + e->event_size;
    uintptr_t last_read_byte = ((uintptr_t) cursor) + data_size;
    assert(last_read_byte <= last_event_byte);

    memcpy(data, cursor, data_size);
    cursor += data_size;
}


/** Explore all the events of all the threads sorted by timestamp*/
void compute_matrix(const pallas::GlobalArchive &trace, uint64_t **&matrix) {
    // For each process
// #pragma omp parallel for default(none) schedule(dynamic) shared(trace, matrix, std::cerr)
    for (uint aid = 0; aid < trace.nb_archives; aid++) {
        auto archive = trace.archive_list[aid];
        /* For each thread in that process */
        for (uint i = 0; i < archive->nb_threads; i++) {
            /* For each event in this thread */
            pallas::Thread *thread = archive->getThreadAt(i);
            if (thread == nullptr) continue;
            /* Get MPI sender rank by getting the LocationGroup id of the thread (LocationGroup = set of threads within a MPI rank) */
            auto *loc = trace.getLocation(thread->id);
            pallas::LocationGroupId sender_rank = loc->parent;
            if (sender_rank != aid) {
// #pragma omp critical
                std::cerr << "Error: P" << aid << "T" << i - aid << " logged message not from current process: "
                          << sender_rank << std::endl;
            }

            for (unsigned j = 0; j < thread->nb_events; j++) {
                pallas::EventSummary& e = thread->events[j];
                /* If the event is a MPI_send */
                if (e.event.record == pallas::PALLAS_EVENT_MPI_SEND ||
                    e.event.record == pallas::PALLAS_EVENT_MPI_ISEND) {
                    byte *cursor = nullptr;

                    /* Get attributes of the MPI call */
                    uint32_t destination;
                    uint32_t communicator;
                    uint32_t msgTag;
                    uint64_t msgLength;

                    pop_data(&e.event, &destination, sizeof(destination), cursor);
                    pop_data(&e.event, &communicator, sizeof(communicator), cursor);
                    pop_data(&e.event, &msgTag, sizeof(msgTag), cursor);
                    pop_data(&e.event, &msgLength, sizeof(msgLength), cursor);

                    /* Update comm matrix for this send between sender and destination rank, with the message size msgLength that happened nb_occurences times
                     * i = sender rank, j = destination rank
                     */
//#pragma omp critical(sender_rank)
                    matrix[sender_rank][destination] += msgLength * e.nb_occurences;
                }
            }
            archive->freeThreadAt(i);
        }
    }
}


void save_matrix(std::string &filename, uint64_t **matrix, size_t size) {
    std::ofstream outfile;
    outfile.open(filename.c_str());

    /* Write the first line containing all the ranks id */
    for (size_t i = 0; i < size; i++) {
        outfile << i;
        if (i < size - 1)
            outfile << ",";
    }
    outfile << std::endl;


    /* Write the content of the matrix rank by rank
     *  i = senders, j = receivers
     */
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            outfile << matrix[i][j];
            if (j < size - 1)
                outfile << ",";
        }
        outfile << std::endl;
    }

    outfile.close();
}


void usage(const std::string &prog_name) {
    std::cout << std::endl << "Usage: " << prog_name << " -t trace_file [OPTION] " << std::endl;
    std::cout << "\t-t FILE   --- Path to the Pallas trace file" << std::endl;
    std::cout
            << "\t[OPTION] -n OUTPUT_FILE   --- Name of the output file. Extension must be .mat. By default, the name of the file with be pallas_comm_matrix.mat"
            << std::endl;
    std::cout << "\t[OPTION] -? -h   --- Display this help and exit" << std::endl;
}


int main(int argc, char **argv) {

    int nb_opts = 0;
    std::string trace_name;
    std::string output_file = "pallas_comm_matrix.mat";

    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-t")) {
            nb_opts++;
            trace_name = std::string(argv[nb_opts + 1]);
            nb_opts++;
        }
        if (!strcmp(argv[i], "-n")) {
            nb_opts++;
            output_file = std::string(argv[nb_opts + 1]);
            nb_opts++;
        } else if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "-?")) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        } else {
            /* Unknown parameter name. Stop parsing the parameter list and breaking. */
            break;
        }
    }


    if (trace_name.empty()) {
        usage(argv[0]);
        return EXIT_SUCCESS;
    }


    /* Starting processing of trace */
    std::cout << "[pallas-comm-matrix] Starting matrix computation..." << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    /* Reading archive */
    pallas::GlobalArchive trace = pallas::GlobalArchive();
    pallasReadGlobalArchive(&trace, const_cast<char *>(trace_name.c_str()));

    /* Create and initialize matrix with dimensions = number of Locations */

    uint size = trace.location_groups.size();
    auto **matrix = new uint64_t *[size];

    for (uint i = 0; i < size; i++) {
        matrix[i] = new uint64_t[size];
    }

    for (uint i = 0; i < size; i++) {
        for (uint j = 0; j < size; j++) {
            matrix[i][j] = 0;
        }
    }


    /* Compute the comm matrix between different MPI processes */
    compute_matrix(trace, matrix);


    /* Save the matrix in the file */
    save_matrix(output_file, matrix, size);

    /* Free memory */
    delete[] (matrix);

    /* Print program duration */
    auto end = std::chrono::high_resolution_clock::now();
    auto compute_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << std::endl << "[pallas-comm-matrix] *** Elapsed time: " << std::setw(7) << compute_duration.count()
              << " nanoseconds. ***" << std::endl;

    return EXIT_SUCCESS;
}

/* -*-
   mode: c;
   c-file-style: "k&r";
   c-basic-offset 2;
   tab-width 2 ;
   indent-tabs-mode nil
   -*- */
